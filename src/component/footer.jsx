import React from "react";
import styled from "styled-components";

const FooterContainer = styled.div`
  margin: 0;
  padding: 0;
  width: 100%;
  position: relative;
  display: flex;
  background-color: yellow;
`;
const InfoContainer = styled.div`
  margin: 0;
  padding: 0;
  width: 30%;

  background-color: white;
`;
const MapContainer = styled.div`
  margin: 0;
  padding: 0;
  width: 30%;

  background-color: blue;
`;
const ContactContainer = styled.div`
  margin: 0;
  padding: 0;
  width: 30%;

  background-color: white;
`;
const Title = styled.h4`
  text-align: center;
`;
const Pragraph = styled.p`
  text-align: center;
`;
const List = styled.ul`
  list-style-type: none;
  display: flex;
  justify-content: space-evenly;
  margin: 0;
  padding: 0;
`;
const Item = styled.li``;
const Icon = styled.img`
  width: 40px;
  color: blue;
`;
const Footer = () => {
  return (
    <FooterContainer>
      <InfoContainer>
        <Title>Info</Title>
        <Pragraph>
          60 metres to the lahdmim Square. Taxi access is 150m away. 1 minutes of
          walking to the souks.
        </Pragraph>
        <List>
          <Item>
            <Icon src="/image/icons/instagram.png" />
          </Item>
          <Item>
            <Icon src="/image/icons/facebook.png" />
          </Item>
          <Item>
            <Icon src="/image/icons/twitter.png" />
          </Item>
        </List>
      </InfoContainer>
      <MapContainer>
        <Title>Map</Title>
      </MapContainer>
      <ContactContainer>
        <Title>Contact</Title>
      </ContactContainer>
    </FooterContainer>
  );
};
export default Footer;
