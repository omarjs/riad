import React from "react";
import Carousel from "nuka-carousel";
import { useState, useEffect } from "react";

const Slider = () => {
  const [height, setheight] = useState("200px");

  return (
    <Carousel initialSlideHeight={height}>
      <img
        src="/image/riad.jpg"
        style={{ height: "600px", objectFit: "cover" }}
      />
      <img src="/image/riad1.jpg" style={{ height: "600px" }} />
      <img src="/image/riad2.jpg" style={{ height: "600px" }} />
    </Carousel>
  );
};

export default Slider;
