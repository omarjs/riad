import React from "react";
import styled from "styled-components";
import { useState, useEffect } from "react";

const Container = styled.div`
  @media (max-width: 650px) {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    font-size: 18px;
    border: 1px solid rgba(0, 0, 0, 0.2);
    padding-bottom: 20px;
    display: block;
  }
  display: flex;
  width: 100%;
`;
const List = styled.ul`
  @media (max-width: 650px) {
    list-style-type: none;
    display: ${props => (props.statu === true ? "none" : "block")};
    margin: 0;
    padding: 0;
    top: 20px;
    width: 100%;
  }
  display: flex;
  list-style-type: none;
  margin: 0;
  width: 100%;
`;
const Item = styled.li`
  @media (max-width: 650px) {
    text-align: center;
    margin: 15px auto;
    height: 35px;

    padding: 13px 0;

    :hover {
      background-color: #fee9c7;
      cursor: pointer;
    }
  }
  :hover {
    background-color: #f69e6e;
  }
  width: 13%;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const LogoLink = styled.a`
  text-decoration: none;
  width: 20%;
`;
const ItemLink = styled.a`
  text-decoration: none;
  ${Item}:hover & {
    color: white;
  }
`;
const ToggleContainer = styled.span``;
const ToggleImage = styled.img`
  @media (max-width: 650px) {
    width: 20px;
    height: 20px;
    position: absolute;
    top: 10px;
    right: 25px;
    cursor: pointer;
    width: 20%;
    display: block;
  }
  display: none;
`;
const LogoImage=styled.img`width:50px;
height:50px;
marging-left:80px`;

const Header = () => {
  const [etat, setEtat] = useState(true);
  useEffect(() => {
    console.log(etat);
  });

  return (
    <Container>
      <ToggleContainer>
        <ToggleImage
          onClick={() => {
            setEtat(!etat);
          }}
          src='/image/bars.svg'
        />
      </ToggleContainer>
      <LogoLink href="#"><LogoImage src="image/logo.png"></LogoImage></LogoLink>
      <List statu={etat}>
        <Item>
          <ItemLink href="#">Home</ItemLink>
        </Item>
        <Item>
          <ItemLink href="#">Galerie</ItemLink>
        </Item>
        <Item>
          <ItemLink href="#">Cuisine</ItemLink>
        </Item>
        <Item>
          <ItemLink href="#">Localisation</ItemLink>
        </Item>
        <Item>
          <ItemLink href="#">Tarif</ItemLink>
        </Item>
        <Item>
          <ItemLink href="#">Reservation</ItemLink>
        </Item>
      </List>
    </Container>
  );
};
export default Header;
