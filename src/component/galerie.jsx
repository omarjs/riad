import React from "react";
import styled from "styled-components";
import rooms from "../data-room/room";
import Room from "./roompicture";
const GalerieContainer = styled.div`
  margin: 0;
  padding: 0;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  background-color: #f5f5f5;
  justify-content: space-evenly;
  position: relative;
  @media (max-width: 600px) {
    flex-direction: column;
    align-content: center;
  }
`;
const Title = styled.h3`
  color: #3c3648;
  text-align: center;
  font-size: 18px;
  text-align: center;
  text-transform: uppercase;
  letter-spacing: 2px;
  font-family: "Poppins", sans-serif;
  margin-top: 2%;
`;
const Galerie = () => {
  return (
    <React.Fragment>
      <Title>Our Rooms</Title>
      <GalerieContainer>
        {rooms.map((room, index) => {
          return <Room key={index} {...room} />;
        })}
      </GalerieContainer>
    </React.Fragment>
  );
};
export default Galerie;
