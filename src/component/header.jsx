import React, { Component } from "react";
import styled from "styled-components";
import { useState } from "react";
const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  margin: 0%;
  padding: 0%;
  background-color: yellow;
`;
const Image = styled.img`
  width: 60px;
  height: 60px;
  margin: 0%;
  padding: 0%;
`;
const LogoContainer = styled.div`
  width: 15%;
  margin: 0%;
  padding: 0%;
  justify-content: center;
  display: flex;
`;
const ListContainer = styled.div`
  width: 70%;
  margin: 0%;
  padding: 0%;
  background-color: blue;
`;
const LanguageContainer = styled.div`
  width: 15%;
`;
const List = styled.ul`
  display: flex;
  list-style-type: none;
  margin: 7px 0px;
  height: 50px;
  padding: 0;
  justify-content: center;
`;
const Item = styled.li`
  background-color: white;
  padding: 2% 0px;
  :hover {
    background-color: #9b7a50;
  }
`;
const Link = styled.a`
  padding: 0 38px;
  text-decoration: none;
  :hover {
    color: white;
  }

  font-size: 17px;
  font-weight: 500;

  list-style: none;
  color: black;
`;

const Header = () => {
  return (
    <HeaderContainer>
      <LogoContainer>
        <Image src="/image/logo.png" />
      </LogoContainer>
      <ListContainer>
        <List>
          <Item>
            <Link href="#">Home</Link>
          </Item>
          <Item>
            <Link href="#">Galerie</Link>
          </Item>
          <Item>
            <Link href="#">Cuisine</Link>
          </Item>
          <Item>
            <Link href="#">Localisation</Link>
          </Item>
          <Item>
            <Link href="#">Tarif</Link>
          </Item>
        </List>
      </ListContainer>
      <LanguageContainer />
    </HeaderContainer>
  );
};
export default Header;
