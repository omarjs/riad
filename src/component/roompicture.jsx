import React from "react";
import styled from "styled-components";

const PictureContainer = styled.div`
  width: 30%;
  border-radius: 5px;
  margin-top: 2%;
  background-color: white;
  @media (max-width: 640px) {
    border-radius: 5px;
    margin-top: 2%;
  }
`;
const Image = styled.img`
  width: 100%;
  object-fit: cover;
  border-radius: 5px;
`;
const Title = styled.h4`
  text-align: center;
`;
const RoomPicture = props => {
  return (
    <PictureContainer>
      <Image src={props.src} />
      <Title>{props.title}</Title>
    </PictureContainer>
  );
};
export default RoomPicture;
