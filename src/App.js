import React from "react";
import Header from "./component/responsiveheader";
import Slider from "./component/slider";
import styled from "styled-components";
import Galerie from "./component/galerie";
import Footer from "./component/footer";

function App() {
  return (
    <React.Fragment>
      <Header />
      <Slider />
      <Galerie />
      <Footer />
    </React.Fragment>
  );

}

export default App;
